package com.thearaseng.oauth2provider.client;

public enum  ClientType {
    PUBLIC, CONFIDENTIAL
}
