package com.thearaseng.oauth2provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OAuthProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(OAuthProviderApplication.class, args);
	}
}
